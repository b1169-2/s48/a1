import { Container, Row, Form, Col, Button } from "react-bootstrap";
import { useState, useEffect } from "react";
import { Navigate, useNavigate} from "react-router-dom";

export default function Login(){

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [isActive, setIsActive] = useState(true)

    function loginUser(e){
        // e.preventDefault()

        console.log(email)

        localStorage.setItem("email", email)

        setEmail("")
        setPassword("")

        alert("Successfully logged in")


    }

    useEffect( () => {
        
        if(email != "" && password != ""){
                setIsActive(false)
            }

    }, [email, password])

    return(
        <Container>
        <Row className="justify-content-center">
            <Col xs={10} md={8}>
                <h1>Login</h1>
                <Form onSubmit={(e) => loginUser(e)}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email" 
                            value={email}
                            onChange={ (e) => {
                                setEmail(e.target.value)
                            }}
                        />
                        <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                        type="password" 
                        placeholder="Password" 
                        value={password}
                        onChange={ (e) => {
                            setPassword(e.target.value)
                        }}
                        />
                    </Form.Group>
                    <Button variant="primary" type="submit" disabled={isActive}>
                        Submit
                    </Button>
                </Form>
            </Col>
        </Row>
    </Container>
    )
}