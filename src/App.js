import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import './App.css';

import AppNavbar from './components/AppNavbar'
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import { Routes} from 'react-router-dom';
import { Route } from 'react-router-dom';
import { BrowserRouter } from 'react-router-dom';
import ErrorPage from './components/ErrorPage';

function App() {

  return(
      <BrowserRouter>
        <AppNavbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/courses" element={<Courses/>} />
          <Route path="/register" element={<Register/>} />
          <Route path="/login" element={<Login/>} />
          <Route path="*" element={<ErrorPage />} />
          
          </Routes>
      </BrowserRouter> 
  )

}

export default App;
